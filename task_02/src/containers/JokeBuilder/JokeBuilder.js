import React, {Component} from 'react';
import Jokes from "../../components/Jokes/Jokes";

class JokeBuilder extends Component {
  state = {
    jokes : []
  };

  componentDidMount(){
    this.getJokeHandler();
  }

  getJokeHandler = () => {
    let promises = [];
    for(let i = 0; i < 5; i++){
      promises.push(
          fetch('https://api.chucknorris.io/jokes/random').then(response => {
            if (response.ok) {
              return response.json();
            }
            throw new Error('Something went wrong with network request');
          })
      )
    }
    
    Promise.all(promises).then(jokes =>{
      
      console.log(jokes);

      const copyJokes = [...this.state.jokes];

      jokes.forEach(joke => {
        let jokeItem = {
          id: joke.id,
          value: joke.value
        };
        copyJokes.push(jokeItem);
      });
      this.setState({jokes: copyJokes});
    }).catch(error => {
      console.log(error);
    })
  };

  render() {
    return (
        <div>
          <Jokes
              jokes={this.state.jokes}
              getJokeHandler={this.getJokeHandler}
          />
        </div>
    );
  }
}

export default JokeBuilder;