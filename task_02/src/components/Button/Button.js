import React, {Component} from 'react';
import './Button.css';

class Button extends Component {
  shouldComponentUpdate(newProps, nextState) {
    return false;
  }

  render() {
    return (
        <div className="joke__add">
          <button className="joke__btn" onClick={this.props.clicked}>
            Добавить шутку
          </button>
          <h3 className="joke__desc">Ну-ка шуткани!</h3>
        </div>

    );
  }
};

export default Button;