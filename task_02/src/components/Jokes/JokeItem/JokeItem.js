import React, {Component} from 'react';

class JokeItem extends Component {
  render(){
    return (
        <div className="joke__item">
          {this.props.text}
        </div>
    );
  }
}

export default JokeItem;