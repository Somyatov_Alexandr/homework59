import React from 'react';
import JokeItem from "./JokeItem/JokeItem";
import Button from "../Button/Button";
import './Jokes.css';

const Jokes = props => {
  const jokesItem = props.jokes.map(joke => {
    return <JokeItem key={joke.id} text={joke.value}/>
  });
  return (
      <div className="jokes">
        <Button clicked={() => props.getJokeHandler()}/>
        <div className="joke__list">
          {jokesItem}
        </div>
      </div>
  );
};

export default Jokes;