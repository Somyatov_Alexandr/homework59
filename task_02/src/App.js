import React, { Component } from 'react';
import JokeBuilder from "./containers/JokeBuilder/JokeBuilder";

class App extends Component {
  render() {
    return (
      <div className="App">
        <JokeBuilder/>
      </div>
    );
  }
}

export default App;
