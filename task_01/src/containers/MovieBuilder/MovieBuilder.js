import React, { Component } from 'react';
import MovieAdd from "../../components/MovieAdd/MovieAdd";
import MovieList from "../../components/MovieList/MovieList";
import './MovieBuilder.css';

class MovieBuilder extends Component {
  state = {
    movies: [],
    movieAdded: ''
  };

  // movieAdded = {};

  handleSubmit = event => {
    event.preventDefault();

    let moviesCopy = [...this.state.movies];

    let movieItem = {
      id: Date.now(),
      title: this.state.movieAdded
    };
    moviesCopy.push(movieItem);

    this.setState({movies: moviesCopy});
    event.target.reset();
  };

  handleMovieTitle = event => {
    this.setState({movieAdded : event.target.value});

    // console.log(this.movieAdded);
  };

  removeMovieItem = title => {
    let moviesCopy = [...this.state.movies];
    const index = moviesCopy.findIndex(item => item.title === title);
    if (index !== -1) {
      moviesCopy.splice(index, 1);
    }

    this.setState({movies: moviesCopy});
  };

  handleChangeMovieItem = (event, id) => {
    let moviesCopy = [...this.state.movies];
    const index = moviesCopy.findIndex(item => item.id === id);

    let movieCopy = moviesCopy[index];
    movieCopy.title = event.target.value;
    moviesCopy[index] = movieCopy;

    this.setState({movies: moviesCopy});
  };

  render() {
    return (
        <div className="movie">
          <MovieAdd
              handleSubmit={this.handleSubmit}
              handleMovieTitle={this.handleMovieTitle}
          />
          <MovieList
              movies={this.state.movies}
              removeItem={this.removeMovieItem}
              changeItem={this.handleChangeMovieItem}
          />
        </div>
    );
  }
}

export default MovieBuilder;