import React, { Component } from 'react';

class MovieItem extends Component {


  shouldComponentUpdate(newProps, newState) {
    return newProps.title !== this.props.title ||
        newProps.id !== this.props.id;
  }


  render() {
    return (
        <div className="movie__item">
          <input
              type="text"
              className="movie__name"
              value={this.props.title}
              onChange={this.props.change}
          />
          <button className="movie__remove" onClick={this.props.remove}>&times;</button>
        </div>
    );
  }
};

export default MovieItem;