import React from 'react';
import './MovieAdd.css';

const MovieAdd = props => {
  return (
      <div className="movie__add">
        <form className="movie__form" onSubmit={event => props.handleSubmit(event)}>
          <input
              type="text"
              className="movie__input"
              placeholder="Add movie, which you want to watch"
              onChange={event => props.handleMovieTitle(event)}
              required
          />
          <button className="movie__btn">Add</button>
        </form>
      </div>
  );
};

export default MovieAdd;