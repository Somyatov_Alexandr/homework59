import React, { Component } from 'react';
import MovieBuilder from "./containers/MovieBuilder/MovieBuilder";

class App extends Component {
  render() {
    return (
      <div className="App">
        <MovieBuilder/>
      </div>
    );
  }
}

export default App;
